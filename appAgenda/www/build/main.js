webpackJsonp([0],{

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CitaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__usuario_service__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CitaService = (function () {
    function CitaService(usuario, http) {
        this.usuario = usuario;
        this.http = http;
        this.url = "http://localhost:3000/api/v1/cita";
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + this.usuario.getToken()
        });
    }
    CitaService.prototype.getCitas = function () {
        return this.http.get(this.url, { headers: this.headers })
            .map(function (res) {
            return res.json();
        });
    };
    CitaService.prototype.getCita = function () {
        var user = JSON.parse(this.usuario.getUsuario());
        var idPerfil = user[0].idPerfil;
        var uri = this.url + "/" + idPerfil;
        return this.http.get(uri, { headers: this.headers })
            .map(function (res) {
            return res.json();
        });
    };
    CitaService.prototype.nuevoCita = function (cita) {
        var data = JSON.stringify(cita);
        return this.http.post(this.url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    CitaService.prototype.editarCita = function (cita) {
        var uri = this.url + "/" + cita.idCita;
        var data = JSON.stringify(cita);
        return this.http.put(uri, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    CitaService.prototype.eliminarCita = function (idCita) {
        var uri = this.url + "/" + idCita;
        return this.http.delete(uri, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    return CitaService;
}());
CitaService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__usuario_service__["a" /* UsuarioService */],
        __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], CitaService);

//# sourceMappingURL=cita.service.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactoForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_contacto_service__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactoForm = (function () {
    function ContactoForm(navCtrl, navParams, toastController, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastController = toastController;
        this.service = service;
        this.estado = false;
        this.contacto = {
            nombre: '',
            apellido: '',
            telefono: '',
            correo: ''
        };
        this.parametro = this.navParams.get('parametro');
        if (this.parametro == 'nuevo') {
            this.titulo = 'Nuevo Contacto';
        }
        else {
            this.titulo = this.parametro.nombre;
            this.contacto = this.parametro;
            this.estado = true;
        }
    }
    ContactoForm.prototype.agregar = function () {
        var _this = this;
        this.service.nuevoContacto(this.contacto)
            .subscribe(function (res) {
            _this.toastController.create({
                message: 'contacto agregado',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this;
            _this.navCtrl.pop();
        }, 1500);
    };
    ContactoForm.prototype.modificar = function () {
        var _this = this;
        this.service.editarContacto(this.contacto)
            .subscribe(function (res) {
            _this.toastController.create({
                message: 'contacto modificado',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    ContactoForm.prototype.eliminar = function () {
        var _this = this;
        this.service.eliminarContacto(this.contacto.idContacto)
            .subscribe(function (res) {
            _this.toastController.create({
                message: 'Contacto Eliminado',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    return ContactoForm;
}());
ContactoForm = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contactoForm',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\contacto\contacto.form.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ titulo }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-list>\n\n          <ion-item>\n\n            <ion-label floating>Nombre</ion-label>\n\n            <ion-input type="text"\n\n            name="nombre"\n\n            [(ngModel)]="contacto.nombre"></ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n            <ion-label floating>Apellido</ion-label>\n\n            <ion-input type="text"\n\n            name="apellido"\n\n            [(ngModel)]="contacto.apellido"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label floating>Correo</ion-label>\n\n            <ion-input type="text"\n\n            name="correo"\n\n            [(ngModel)]="contacto.correo"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label floating>Telefono</ion-label>\n\n            <ion-input type="text"\n\n            name="telefono"\n\n            [(ngModel)]="contacto.telefono"></ion-input>\n\n          </ion-item>\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <button *ngIf="!estado" outline ion-button block color="primary"\n\n        (click)="agregar()">\n\n        Guardar\n\n      </button>\n\n        <button *ngIf="estado" outline (click)="modificar()" ion-button block color="primary">Guardar</button>\n\n        <button *ngIf="estado" outline (click)="eliminar()" ion-button block color="danger">Eliminar</button>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\contacto\contacto.form.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__app_services_contacto_service__["a" /* ContactoService */]])
], ContactoForm);

//# sourceMappingURL=contacto.form.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TareaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__usuario_service__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TareaService = (function () {
    function TareaService(usuario, http) {
        this.usuario = usuario;
        this.http = http;
        this.url = "http://localhost:3000/api/v1/tarea";
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + this.usuario.getToken()
        });
    }
    TareaService.prototype.getTareas = function () {
        return this.http.get(this.url, { headers: this.headers })
            .map(function (res) {
            return res.json();
        });
    };
    TareaService.prototype.getTarea = function () {
        var user = JSON.parse(this.usuario.getUsuario());
        var idPerfil = user[0].idPerfil;
        var uri = this.url + "/" + idPerfil;
        return this.http.get(uri, { headers: this.headers })
            .map(function (res) {
            return res.json();
        });
    };
    TareaService.prototype.nuevoTarea = function (tarea) {
        var user = JSON.parse(this.usuario.getUsuario());
        var idPerfil = user[0].idPerfil;
        tarea.id = idPerfil;
        var data = JSON.stringify(tarea);
        return this.http.post(this.url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    TareaService.prototype.editarTarea = function (tarea) {
        var uri = this.url + "/" + tarea.idTarea;
        var data = JSON.stringify(tarea);
        return this.http.put(uri, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    TareaService.prototype.eliminarTarea = function (idTarea) {
        var uri = this.url + "/" + idTarea;
        return this.http.delete(uri, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    return TareaService;
}());
TareaService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__usuario_service__["a" /* UsuarioService */],
        __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], TareaService);

//# sourceMappingURL=tarea.service.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_usuario_service__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_tabs_tabs__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(262);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, toast, service, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.usuario = {
            nick: '',
            password: ''
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.iniciarSesion = function () {
        var _this = this;
        this.service.autenticacion(this.usuario)
            .subscribe(function (res) {
            if (!res.estado) {
                _this.toast.create({
                    message: "usuario o contrasena invalida",
                    duration: 1500
                }).present();
                _this.usuario.nick = '',
                    _this.usuario.password = '';
            }
            else {
                var loader = _this.loadingCtrl.create({
                    content: "Please wait...",
                    duration: 1000
                });
                loader.present();
                setTimeout(function () {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_tabs_tabs__["a" /* TabsPage */], {}, { animate: true });
                }, 1500);
            }
        });
    };
    LoginPage.prototype.registrar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */], {}, { animate: true });
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\login\login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content class="con" padding >\n  \n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <h1>Bienvenido</h1>\n        <ion-list>\n          <ion-item>\n            <ion-label floating>Username</ion-label>\n            <ion-input name="nick" [(ngModel)]="usuario.nick" type="text"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-label floating>Password</ion-label>\n            <ion-input name="password" [(ngModel)]="usuario.password" type="password"></ion-input>\n          </ion-item>\n        </ion-list>\n        <button ion-button block text-capitalize (click)="iniciarSesion()">Log In</button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <button ion-button clear (click)="registrar()" text-capitalize>Registrate</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__app_services_usuario_service__["a" /* UsuarioService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 146:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 146;

/***/ }),

/***/ 189:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 189;

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_cita_service__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contacto_contacto_form__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cita_form__ = __webpack_require__(258);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AboutPage = (function () {
    function AboutPage(navCtrl, service) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.citas = [];
    }
    AboutPage.prototype.ionViewDidEnter = function () {
        this.cargarCitas();
    };
    AboutPage.prototype.cargarCitas = function () {
        var _this = this;
        this.service.getCita()
            .subscribe(function (res) {
            _this.citas = res[0];
        });
    };
    AboutPage.prototype.mostarContacto = function (item) {
        var parametro = {
            nombre: item.nombre,
            apellido: item.apellido,
            telefono: item.telefono,
            correo: item.correo,
            idContacto: item.idContacto
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__contacto_contacto_form__["a" /* ContactoForm */], { parametro: parametro });
    };
    AboutPage.prototype.cargar = function (parametro) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cita_form__["a" /* CitaForm */], { parametro: parametro });
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\cita\about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Mis Citas\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="presentConfirm()">\n        <ion-icon ios="ios-log-out" md="ios-log-out"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card *ngFor="let cita of citas">\n\n  <ion-item>\n    <ion-avatar item-start>\n      <img (click)="mostarContacto(cita)" \n        src="https://www.mycustomer.com/sites/all/modules/custom/sm_pp_user_profile/img/default-user.png">\n    </ion-avatar>\n    <h2>{{ cita.nombre }}</h2>\n    <p>{{ cita.fecha | date: \'longDate\' }}</p>\n  </ion-item>\n  <div (press)="cargar(cita)">\n    <img src="https://ionicframework.com/dist/preview-app/www/assets/img/advance-card-bttf.png">\n    <ion-card-content>\n      <p>{{ cita.descripcion }}</p>\n    </ion-card-content>\n  </div>\n  </ion-card>\n\n   <ion-fab right bottom>\n    <button ion-fab (click)="cargar(\'nuevo\')" color="light"><ion-icon name="add"></ion-icon></button>\n  </ion-fab>\n\n</ion-content>\n'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\cita\about.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__app_services_cita_service__["a" /* CitaService */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CitaForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_cita_service__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_contacto_service__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CitaForm = (function () {
    function CitaForm(navCtrl, service, contactoService, navParams, toast) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.service = service;
        this.contactoService = contactoService;
        this.navParams = navParams;
        this.toast = toast;
        this.cita = {
            descripcion: '',
            fecha: '',
            lugar: '',
            id: ''
        };
        this.status = false;
        this.contactos = [];
        this.parametro = this.navParams.get('parametro');
        if (this.parametro == 'nuevo') {
            this.titulo = 'Nueva Cita';
        }
        else {
            this.titulo = this.parametro.titulo;
            this.cita = this.parametro;
            this.cita.id = this.cita.idContacto;
            this.status = true;
        }
        this.contactoService.getContacto()
            .subscribe(function (res) {
            _this.contactos = res;
        });
    }
    CitaForm.prototype.agregar = function () {
        var _this = this;
        this.service.nuevoCita(this.cita)
            .subscribe(function (res) {
            _this.toast.create({
                message: 'Cita Agregada',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    CitaForm.prototype.modificar = function () {
        var _this = this;
        this.service.editarCita(this.cita)
            .subscribe(function (res) {
            _this.toast.create({
                message: 'Cita Modificada',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    CitaForm.prototype.eliminar = function () {
        var _this = this;
        this.service.eliminarCita(this.cita.idCita)
            .subscribe(function (res) {
            _this.toast.create({
                message: 'Cita Eliminada',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    return CitaForm;
}());
CitaForm = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-citaForm',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\cita\cita.form.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ titulo }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-list>            \n\n          <ion-item>\n\n            <ion-label floating>Descripcion</ion-label>\n\n            <ion-input type="text"\n\n            name="descripcion"\n\n            [(ngModel)]="cita.descripcion"></ion-input>\n\n          </ion-item>\n\n          <ion-item-divider>\n\n                <ion-label floating>Fecha</ion-label>\n\n                <ion-datetime displayFormat="YYYY/MM/DD" [(ngModel)]="cita.fecha"></ion-datetime>\n\n          </ion-item-divider>\n\n            <ion-item-divider>\n\n                <ion-label floating>Lugar</ion-label>\n\n                <ion-input type="text"\n\n            name="descripcion"\n\n            [(ngModel)]="cita.lugar"></ion-input>\n\n            </ion-item-divider>\n\n            \n\n            <ion-list>\n\n            <ion-item>\n\n                <ion-label floating>Contaco</ion-label>\n\n                <ion-select [(ngModel)]="cita.id">\n\n                <ion-option *ngFor="let contacto of contactos" [value]="contacto.idContacto">\n\n                    {{ contacto.nombre }}\n\n                </ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n            </ion-list>\n\n\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <button *ngIf="!status" ion-button block outline color="primary"\n\n        (click)="agregar()">\n\n        Guardar\n\n      </button>\n\n        <button *ngIf="status" (click)="modificar()" ion-button block outline color="primary">Guardar</button>\n\n        <button *ngIf="status" (click)="eliminar()" ion-button block outline color="danger">Eliminar</button>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\cita\cita.form.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__app_services_cita_service__["a" /* CitaService */],
        __WEBPACK_IMPORTED_MODULE_3__app_services_contacto_service__["a" /* ContactoService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
], CitaForm);

//# sourceMappingURL=cita.form.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_contacto_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contacto_form__ = __webpack_require__(133);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactPage = (function () {
    function ContactPage(navCtrl, contacto) {
        this.navCtrl = navCtrl;
        this.contacto = contacto;
        this.contactos = [];
    }
    ContactPage.prototype.ionViewDidEnter = function () {
        this.cargarContactos();
    };
    ContactPage.prototype.evenTap = function (event) {
        console.log(event);
    };
    ContactPage.prototype.cargarContactos = function () {
        var _this = this;
        this.contacto.getContacto()
            .subscribe(function (res) {
            _this.contactos = res;
        });
    };
    ContactPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.cargarContactos();
            refresher.complete();
        }, 1000);
    };
    ContactPage.prototype.cargarFormulario = function (parametro) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__contacto_form__["a" /* ContactoForm */], { parametro: parametro });
    };
    ContactPage.prototype.evento = function (parametro) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__contacto_form__["a" /* ContactoForm */], { parametro: parametro });
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\contacto\contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Mis Contactos\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="presentConfirm()">\n        <ion-icon ios="ios-log-out" md="ios-log-out"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingIcon="arrow-dropdown"\n      pullingText="Pull to refresh"\n      refreshingSpinner="circles"\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n <ion-list>\n  <ion-item *ngFor = "let contacto of contactos" detail-push>\n    <ion-avatar item-start>\n      <img src="https://www.mycustomer.com/sites/all/modules/custom/sm_pp_user_profile/img/default-user.png">\n    </ion-avatar>\n    <h2>{{ contacto.nombre }}</h2>\n    <p>{{ contacto.telefono }}</p>\n    <button ion-button outline (click)="evento(contacto)" item-end>View</button>\n  </ion-item>\n</ion-list>\n  <ion-fab right bottom>\n    <button ion-fab (click)="cargarFormulario(\'nuevo\')" color="light"><ion-icon name="add"></ion-icon></button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\contacto\contact.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__app_services_contacto_service__["a" /* ContactoService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__app_services_contacto_service__["a" /* ContactoService */]) === "function" && _b || Object])
], ContactPage);

var _a, _b;
//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_tarea_service__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_usuario_service__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_form__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = (function () {
    function HomePage(navCtrl, service, usuarioService, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.usuarioService = usuarioService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.tareas = [];
    }
    HomePage.prototype.ionViewDidEnter = function () {
        this.cargarTareas();
    };
    HomePage.prototype.cargar = function (parametro) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_form__["a" /* HomeForm */], { parametro: parametro });
    };
    HomePage.prototype.cargarTareas = function () {
        var _this = this;
        this.service.getTarea()
            .subscribe(function (res) {
            _this.tareas = res;
        });
    };
    HomePage.prototype.presentConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cerrar Session',
            message: '¿Seguro que desea cerrar la session?',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Confirmar',
                    handler: function () {
                        _this.usuarioService.destroyeToken();
                        var loader = _this.loadingCtrl.create({
                            content: "Please wait...",
                            duration: 1000
                        });
                        loader.present();
                        setTimeout(function () {
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */], {}, { animate: true });
                        }, 1500);
                    }
                }
            ]
        });
        alert.present();
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\tarea\home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Mis Tareas</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="presentConfirm()">\n        <ion-icon ios="ios-log-out" md="ios-log-out"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-card *ngFor= "let tarea of tareas" (press)="cargar(tarea)">\n      <ion-card-header>\n        {{ tarea.titulo }}\n      </ion-card-header>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <p>{{ tarea.descripcion }}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            {{ tarea.fin | date: "dd/MM/y" }}\n          </ion-col>\n          <ion-col>\n            <ion-badge icon-rigth>{{ tarea.estado }}</ion-badge>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n    </ion-card>\n <ion-fab right bottom>\n    <button ion-fab (click)="cargar(\'nuevo\')" color="light"><ion-icon name="add"></ion-icon></button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\tarea\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__app_services_tarea_service__["a" /* TareaService */],
        __WEBPACK_IMPORTED_MODULE_3__app_services_usuario_service__["a" /* UsuarioService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 261:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_tarea_service__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeForm = (function () {
    function HomeForm(navCtrl, service, navParams, toast) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.navParams = navParams;
        this.toast = toast;
        this.tarea = {
            titulo: '',
            descripcion: '',
            fin: '',
            estado: ''
        };
        this.status = false;
        this.parametro = this.navParams.get('parametro');
        if (this.parametro == 'nuevo') {
            this.titulo = 'Nueva Tarea';
        }
        else {
            this.titulo = this.parametro.titulo;
            this.tarea = this.parametro;
            this.status = true;
        }
    }
    HomeForm.prototype.agregar = function () {
        var _this = this;
        this.service.nuevoTarea(this.tarea)
            .subscribe(function (res) {
            _this.toast.create({
                message: 'Tarea Agregada',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    HomeForm.prototype.modificar = function () {
        var _this = this;
        this.service.editarTarea(this.tarea)
            .subscribe(function (res) {
            _this.toast.create({
                message: 'Tarea Modificada',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    HomeForm.prototype.eliminar = function () {
        var _this = this;
        this.service.eliminarTarea(this.tarea.idTarea)
            .subscribe(function (res) {
            _this.toast.create({
                message: 'Tarea Eliminada',
                duration: 1000
            }).present();
        });
        setTimeout(function () {
            _this.navCtrl.pop();
        }, 1500);
    };
    return HomeForm;
}());
HomeForm = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\tarea\home.form.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ titulo }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-list>\n\n          <ion-item>\n\n            <ion-label floating>Titulo</ion-label>\n\n            <ion-input type="text"\n\n            name="titulo"\n\n            [(ngModel)]="tarea.titulo"></ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n            <ion-label floating>Descripcion</ion-label>\n\n            <ion-input type="text"\n\n            name="descripcion"\n\n            [(ngModel)]="tarea.descripcion"></ion-input>\n\n          </ion-item>\n\n          <ion-item-divider>\n\n                <ion-label floating>Entrega</ion-label>\n\n                <ion-datetime displayFormat="YYYY/MM/DD" [(ngModel)]="tarea.fin"></ion-datetime>\n\n          </ion-item-divider>\n\n          <ion-list>\n\n            <ion-item>\n\n                <ion-label floating>Estado</ion-label>\n\n                <ion-select [(ngModel)]="tarea.estado">\n\n                <ion-option value="En Proceso">En Proceso</ion-option>\n\n                <ion-option value="Sin Iniciar">Sin Iniciar</ion-option>\n\n                <ion-option value="Terminada">Terminada</ion-option>\n\n                <ion-option value="Pausada">Pausada</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n            </ion-list>\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <button *ngIf="!status" ion-button block outline color="primary"\n\n        (click)="agregar()">\n\n        Guardar\n\n      </button>\n\n        <button *ngIf="status" (click)="modificar()" ion-button block outline color="primary">Guardar</button>\n\n        <button *ngIf="status" (click)="eliminar()" ion-button block outline color="danger">Eliminar</button>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\tarea\home.form.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__app_services_tarea_service__["a" /* TareaService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
], HomeForm);

//# sourceMappingURL=home.form.js.map

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_usuario_service__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_tabs_tabs__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignupPage = (function () {
    function SignupPage(navCtrl, navParams, toast, loadingCtrl, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.usuario = {
            nombre: '',
            apellido: '',
            correo: '',
            nick: '',
            password: '',
        };
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.iniciarSesion = function () {
        var _this = this;
        this.service.registrar(this.usuario)
            .subscribe(function (res) {
            var loader = _this.loadingCtrl.create({
                content: "Please wait...",
                duration: 1000
            });
            loader.present();
            setTimeout(function () {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_tabs_tabs__["a" /* TabsPage */], {}, { animate: true });
            }, 1500);
        });
    };
    SignupPage.prototype.login = function () {
        this.navCtrl.pop();
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\signup\signup.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Registrate</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-list>\n          <ion-item>\n            <ion-label floating>Nombre</ion-label>\n            <ion-input type="text"\n            name="nombre"\n            [(ngModel)]="usuario.nombre"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-label floating>Apellido</ion-label>\n            <ion-input type="text"\n            name="apellido"\n            [(ngModel)]="usuario.apellido"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating>Correo</ion-label>\n            <ion-input type="text"\n            name="correo"\n            [(ngModel)]="usuario.correo"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating>Nick</ion-label>\n            <ion-input type="text"\n            name="nick"\n            [(ngModel)]="usuario.nick"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating>Password</ion-label>\n            <ion-input type="password"\n            name="password"\n            [(ngModel)]="usuario.password"></ion-input>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n       <button ion-button block text-capitalize (click)="iniciarSesion()">Registrarme</button>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <button ion-button clear (click)="login()" text-capitalize>Ya tengo una Cuenta</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\signup\signup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__app_services_usuario_service__["a" /* UsuarioService */]])
], SignupPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(268);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_cita_about__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_cita_cita_form__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_contacto_contact__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_contacto_contacto_form__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tarea_home__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tarea_home_form__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_login_login__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_signup_signup__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_usuario_service__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_contacto_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_tarea_service__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_cita_service__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_splash_screen__ = __webpack_require__(234);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















// servicios






var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_cita_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_contacto_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_tarea_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_contacto_contacto_form__["a" /* ContactoForm */],
            __WEBPACK_IMPORTED_MODULE_11__pages_tarea_home_form__["a" /* HomeForm */],
            __WEBPACK_IMPORTED_MODULE_7__pages_cita_cita_form__["a" /* CitaForm */],
            __WEBPACK_IMPORTED_MODULE_14__pages_signup_signup__["a" /* SignupPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */]),
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_cita_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_contacto_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_tarea_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_contacto_contacto_form__["a" /* ContactoForm */],
            __WEBPACK_IMPORTED_MODULE_11__pages_tarea_home_form__["a" /* HomeForm */],
            __WEBPACK_IMPORTED_MODULE_7__pages_cita_cita_form__["a" /* CitaForm */],
            __WEBPACK_IMPORTED_MODULE_14__pages_signup_signup__["a" /* SignupPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_19__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_15__services_usuario_service__["a" /* UsuarioService */],
            __WEBPACK_IMPORTED_MODULE_16__services_contacto_service__["a" /* ContactoService */],
            __WEBPACK_IMPORTED_MODULE_17__services_tarea_service__["a" /* TareaService */],
            __WEBPACK_IMPORTED_MODULE_18__services_cita_service__["a" /* CitaService */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_usuario_service__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, usaurioService) {
        this.usaurioService = usaurioService;
        if (usaurioService.isLogged()) {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        }
        else {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
        }
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_6__services_usuario_service__["a" /* UsuarioService */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsuarioService = (function () {
    function UsuarioService(http) {
        this.http = http;
        this.url = "http://localhost:3000";
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
    }
    UsuarioService.prototype.registrar = function (usuario) {
        var _this = this;
        var uri = this.url + "/api/v1/usuario";
        var data = JSON.stringify(usuario);
        return this.http.post(uri, data, { headers: this.headers })
            .map(function (res) {
            if (res.json().estado) {
                _this.setToken(res.json().token);
                _this.setUsuario(res.json().usuario);
            }
            return res.json();
        });
    };
    UsuarioService.prototype.autenticacion = function (usuario) {
        var _this = this;
        var uri = this.url + "/auth";
        var data = JSON.stringify(usuario);
        return this.http.post(uri, data, { headers: this.headers })
            .map(function (res) {
            if (res.json().estado) {
                _this.setToken(res.json().token);
                _this.setUsuario(res.json().usuario);
            }
            return res.json();
        });
    };
    UsuarioService.prototype.setToken = function (token) {
        localStorage.setItem('TOKEN', token);
    };
    UsuarioService.prototype.getToken = function () {
        return localStorage.getItem('TOKEN');
    };
    UsuarioService.prototype.setUsuario = function (usuario) {
        localStorage.setItem('USUARIO', JSON.stringify(usuario));
    };
    UsuarioService.prototype.getUsuario = function () {
        return localStorage.getItem('USUARIO');
    };
    UsuarioService.prototype.destroyeToken = function () {
        localStorage.clear();
    };
    UsuarioService.prototype.isLogged = function () {
        if (localStorage.getItem('TOKEN')) {
            return true;
        }
        return false;
    };
    return UsuarioService;
}());
UsuarioService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], UsuarioService);

//# sourceMappingURL=usuario.service.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cita_about__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contacto_contact__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tarea_home__ = __webpack_require__(260);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_4__tarea_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__cita_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__contacto_contact__["a" /* ContactPage */];
    }
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Ionic\Agenda\appAgenda\src\pages\tabs\tabs.html"*/'<ion-tabs >\n  <ion-tab [root]="tab1Root" tabTitle="Tareas" tabIcon="md-clipboard"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Citas" tabIcon="ios-bookmarks-outline"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Contactos" tabIcon="contacts"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"C:\Ionic\Agenda\appAgenda\src\pages\tabs\tabs.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__usuario_service__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactoService = (function () {
    function ContactoService(usuario, http) {
        this.usuario = usuario;
        this.http = http;
        this.url = "http://localhost:3000/api/v1/contacto";
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + this.usuario.getToken()
        });
    }
    ContactoService.prototype.getContactos = function () {
        return this.http.get(this.url, { headers: this.headers })
            .map(function (res) {
            return res.json();
        });
    };
    ContactoService.prototype.getContacto = function () {
        var user = JSON.parse(this.usuario.getUsuario());
        var idPerfil = user[0].idPerfil;
        var uri = this.url + "/" + idPerfil;
        return this.http.get(uri, { headers: this.headers })
            .map(function (res) {
            return res.json();
        });
    };
    ContactoService.prototype.nuevoContacto = function (contacto) {
        var user = JSON.parse(this.usuario.getUsuario());
        var idPerfil = user[0].idPerfil;
        contacto.id = idPerfil;
        var data = JSON.stringify(contacto);
        return this.http.post(this.url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ContactoService.prototype.editarContacto = function (contacto) {
        var uri = this.url + "/" + contacto.idContacto;
        var data = JSON.stringify(contacto);
        return this.http.put(uri, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ContactoService.prototype.eliminarContacto = function (idContacto) {
        var uri = this.url + "/" + idContacto;
        return this.http.delete(uri, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    return ContactoService;
}());
ContactoService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__usuario_service__["a" /* UsuarioService */],
        __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ContactoService);

//# sourceMappingURL=contacto.service.js.map

/***/ })

},[263]);
//# sourceMappingURL=main.js.map